const Url = "http://localhost:8181/employees";
let jsonReq = "";
let jsonDel = "";


      const getData = () => {
            fetch(`${Url}`)
            .then((response) => {
                  return response.json();
            })
            .then((responseJson) => {
                  renderData(responseJson);

            })
            .catch((error) => {
                  console.log(error);
            });
      }

      const renderData = results => {
            const tbody = document.querySelector("#tbody");
            tbody.innerHTML = "";
            
            results.forEach( res => {
                  tbody.innerHTML += `
                  <tr>
                        <td>${res.nik}</td>
                        <td>${res.name}</td>
                        <td>${res.pob}</td>
                        <td>${res.bd}</td>
                        <td>${res.gender}</td>
                        <td>${res.address}</td>
                        <td>${res.religion}</td>
                        <td>${res.profession}</td>
                        <td>${res.citizenship}</td>
                        <td><button type="button" id="btndetail" class="btn btn-primary btn-sm"><i class="fa fa-address-card" aria-hidden="true"></i></button>
                        <button type="button" id="btnupdate" class="btn btn-primary btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                        <button type="button" id="btndelete" class="btn btn-primary btn-sm" onclick='deleteData(${res.id})'><i class="fa fa-trash" aria-hidden="true"></i></button><td>
                  </tr>`;
            });
      }

     getData();

     
        
//      document.querySelector("#btndelete").onclick = function() {
            
//       const nik = document.getElementById("nik").value;
      
//             jsonDel = JSON.stringify({
//                   nik: nik
//             })
//           deleteData(jsonDel);
//   };


  function deleteData(id) {
      fetch(`http://localhost:8181/delete/${id}`, {
          method: 'DELETE',
      })
      .then(res => {        
            alert("Berhasil")
            location.reload()
       })
       
      .catch(console.error);
  }
