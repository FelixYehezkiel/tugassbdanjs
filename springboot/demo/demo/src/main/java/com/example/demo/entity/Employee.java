package com.example.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "EMPLOYEE_MASTER")
public class Employee {
    @Id
    @GeneratedValue
    private int id;
    private String nik;
    private String name;
    private String pob;
    private String bd;
    private String gender;
    private String address;
    private String religion;
    private String profession;
    private String citizenship;
}
