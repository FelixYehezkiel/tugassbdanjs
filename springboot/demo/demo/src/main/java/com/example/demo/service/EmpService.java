package com.example.demo.service;

import com.example.demo.entity.Employee;
import com.example.demo.repository.EmpRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmpService {
    @Autowired
    private EmpRepository repository;

    public Employee saveEmployee(Employee employee) {
        return repository.save(employee);
    }

    public List<Employee> saveEmployees(List<Employee> employees){
        return repository.saveAll(employees);
    }

    public List<Employee> getEmployees() {
        return repository.findAll();
    }

    public Employee getEmployeeById(int id){
        return repository.findById(id).orElse(null);
    }

    public Employee getEmployeeByName(String name) {
        return repository.findByName(name);
    }

    public String deleteEmployee(int id) {
        repository.deleteById(id);
        return "Employee removed!!";
    }

    public Employee updateEmployee(Employee employee) {
        Employee existingEmployee = repository.findById(employee.getId()).orElse(null);
        existingEmployee.setNik(employee.getNik());
        existingEmployee.setName(employee.getName());
        existingEmployee.setPob(employee.getPob());
        existingEmployee.setGender(employee.getGender());
        existingEmployee.setBd(employee.getBd());
        existingEmployee.setAddress(employee.getAddress());
        existingEmployee.setReligion(employee.getReligion());
        existingEmployee.setProfession(employee.getProfession());
        existingEmployee.setCitizenship(employee.getCitizenship());
        return repository.save(existingEmployee);
    }
}
