-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 04 Jul 2020 pada 07.44
-- Versi server: 10.4.13-MariaDB
-- Versi PHP: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `data`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `employee_master`
--

CREATE TABLE `employee_master` (
  `id` int(11) NOT NULL,
  `nik` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `pob` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `bd` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `religion` varchar(255) DEFAULT NULL,
  `profession` varchar(255) DEFAULT NULL,
  `citizenship` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `employee_master`
--

INSERT INTO `employee_master` (`id`, `nik`, `name`, `pob`, `gender`, `bd`, `address`, `religion`, `profession`, `citizenship`) VALUES
(38, '123456789', 'Kak Ocep', 'Semangka', 'Perempuan', '12-Februari-2000', 'Aloha', 'Islam', 'Pilot', 'WNI');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(40);

-- --------------------------------------------------------

--
-- Struktur dari tabel `product_master`
--

CREATE TABLE `product_master` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price` double NOT NULL,
  `qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `product_master`
--

INSERT INTO `product_master` (`id`, `name`, `price`, `qty`) VALUES
(1, 'Mouse', 150000, 5),
(3, 'AAA', 100000, 1),
(4, 'BBB', 120000, 2),
(5, 'CCC', 130000, 3);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `employee_master`
--
ALTER TABLE `employee_master`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `product_master`
--
ALTER TABLE `product_master`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
